from django.contrib import admin
from .models import TodoItem, TodoList

class TodoListAdmin(admin.ModelAdmin):
    list_display = ('name', 'id')

# should manually create item and/or add to list so they function properly
class TodoItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(TodoList, TodoListAdmin)
admin.site.register(TodoItem, TodoItemAdmin)
